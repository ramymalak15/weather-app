package com.demo.weather.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.demo.weather.WeatherApplication
import com.demo.weather.di.component.FragmentComponent
import com.demo.weather.di.component.DaggerFragmentComponent
import com.demo.weather.di.module.FragmentModule
import javax.inject.Inject

abstract class BaseFragment<VM : BaseViewModel, VB : ViewBinding> : Fragment() {

    @Inject
    lateinit var viewModel: VM

    private var _binding: ViewBinding? = null

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    protected abstract fun bindingInflater(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) : VB


    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies(buildFragmentComponent())
        super.onCreate(savedInstanceState)
        setupObservers()
        viewModel.onCreate()
    }

    private fun buildFragmentComponent() =
        DaggerFragmentComponent
            .builder()
            .applicationComponent((requireContext().applicationContext as WeatherApplication).applicationComponent)
            .fragmentModule(FragmentModule(this))
            .build()



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = bindingInflater(inflater, container)
        return requireNotNull(_binding).root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    protected open fun setupObservers() {
        viewModel.messageString.observe(this, {
            it?.run { showMessage(this) }
        })

        viewModel.messageStringId.observe(this, {
            it?.run { showMessage(this) }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
    }


    fun showMessage(message: String) = context?.let { Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show() }

    fun showMessage(@StringRes resId: Int) = showMessage(getString(resId))

    protected abstract fun injectDependencies(fragmentComponent: FragmentComponent)

    protected abstract fun setupView(view: View)
}