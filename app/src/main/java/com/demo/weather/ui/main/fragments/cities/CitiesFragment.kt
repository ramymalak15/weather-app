package com.demo.weather.ui.main.fragments.cities

import android.Manifest
import android.annotation.SuppressLint
import android.content.IntentSender
import android.location.LocationManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.weather.R
import com.demo.weather.databinding.CitiesFragmentBinding
import com.demo.weather.di.component.FragmentComponent
import com.demo.weather.ui.base.BaseFragment
import com.demo.weather.ui.main.fragments.city_item.CityAdapter
import com.demo.weather.utils.Constants
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import javax.inject.Inject
import javax.inject.Provider

class CitiesFragment : BaseFragment<CitiesViewModel, CitiesFragmentBinding>() {

    companion object {
        const val TAG = "CitiesFragment"
    }

    @Inject
    lateinit var citiesRecyclerAdapter: CityAdapter

    @Inject
    lateinit var linearLayoutManager: Provider<LinearLayoutManager>

    @Inject
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    @Inject
    lateinit var locationManager: LocationManager

    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): CitiesFragmentBinding = CitiesFragmentBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) =
        fragmentComponent.inject(this)

    override fun setupView(view: View) {
        binding.selectedCitiesRv.apply {
            adapter = citiesRecyclerAdapter
            layoutManager = linearLayoutManager.get()
        }
        binding.addCityBtn.setOnClickListener {
            findNavController().navigate(R.id.action_citiesFragment_to_searchFragment)
        }
        binding.addCityBtn.isEnabled = citiesRecyclerAdapter.size() < Constants.MAX_CITY
        binding.citiesCounter.text = getString(R.string.selected_cities, citiesRecyclerAdapter.size(), Constants.MAX_CITY)
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.selectedCities.observe(this, {
            if (it.isNullOrEmpty()) {
                viewModel.onEmptyCities()
            } else {
                citiesRecyclerAdapter.updateList(it)
            }
            binding.addCityBtn.isEnabled = it.size < Constants.MAX_CITY
            binding.citiesCounter.text = getString(R.string.selected_cities, it.size, Constants.MAX_CITY)
        })

        viewModel.isLoading.observe(this, {
            binding.progress.visibility = if (it) View.VISIBLE else View.GONE
        })
        viewModel.isEmpty.observe(this, {
            if (it) {
                checkLocationPermissions()
            }
        })

        viewModel.isNetworkConnected.observe(this, {
            if (it == false) {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.internet_not_avaliable))
                    .setPositiveButton(getString(R.string.retry)) { dialog, _ ->
                        dialog.dismiss()
                        checkLocationPermissions()
                    }
                    .setCancelable(false)
                    .show()
            }
        })


    }


    private fun checkLocationPermissions() {
        Dexter.withContext(requireContext())
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (it.deniedPermissionResponses.isNotEmpty()) {
                            // at least one permission is denied
                            viewModel.setDefault()
                        } else {
                            // all permission granted
                            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                getCurrentUserLocation()
                            } else {
                                askToEnableGPS()
                            }
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            })
            .withErrorListener {
                showMessage(it.name)
            }
            .check()
    }

    private fun askToEnableGPS() {
        LocationServices.getSettingsClient(requireActivity())
            .checkLocationSettings(
                LocationSettingsRequest.Builder()
                    .addLocationRequest(LocationRequest.create().apply {
                        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                    }).build()
            )
            .addOnSuccessListener {
                //  GPS is already enable, callback GPS status through listener
                getCurrentUserLocation()
            }
            .addOnFailureListener { e ->
                when ((e as ApiException).statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        // system open location
                        val rae = e as ResolvableApiException
                        launchGPS.launch(IntentSenderRequest.Builder(rae.resolution).build())

                    } catch (sie: IntentSender.SendIntentException) {
                        Log.i(
                            TAG,
                            "PendingIntent unable to execute request."
                        )
                        viewModel.setDefault()
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        Log.i(
                            TAG,
                            "SETTINGS CHANGE UNAVAILABLE"
                        )
                        viewModel.setDefault()
                    }
                }

            }
    }

    private val launchGPS =
        registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            if (result.resultCode == AppCompatActivity.RESULT_OK) {
                getCurrentUserLocation()
            } else {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.gps_in_need))
                    .setPositiveButton(getString(R.string.confirm)) { dialog, _ ->
                        dialog.dismiss()
                        viewModel.onEmptyCities()
                    }
                    .setNegativeButton(getString(R.string.get_default)) { dialog, _ ->
                        dialog.dismiss()
                        viewModel.setDefault()
                    }
                    .setCancelable(false)
                    .show()
            }
        }

    @SuppressLint("MissingPermission")
    fun getCurrentUserLocation() {
        fusedLocationProviderClient.getCurrentLocation(
            LocationRequest.PRIORITY_HIGH_ACCURACY,
            object : CancellationToken() {
                override fun isCancellationRequested(): Boolean = false

                override fun onCanceledRequested(p0: OnTokenCanceledListener): CancellationToken =
                    this

            }
        )
            .addOnSuccessListener {
                viewModel.setSelectedByLocation(it.longitude, it.latitude)

            }
            .addOnFailureListener {
                MaterialAlertDialogBuilder(requireContext())
                    .setMessage(getString(R.string.failed_to_get_current_location))
                    .setPositiveButton(getString(R.string.retry)) { dialog, _ ->
                        dialog.dismiss()
                        viewModel.onEmptyCities()
                    }
                    .setNegativeButton(getString(R.string.get_default)) { dialog, _ ->
                        dialog.dismiss()
                        viewModel.setDefault()
                    }
                    .setCancelable(false)
                    .show()
            }
    }

}