package com.demo.weather.ui.main.fragments.search

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.demo.weather.R
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.data.repository.CitiesRepository
import com.demo.weather.ui.base.BaseViewModel
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class SearchViewModel(
    compositeDisposable: CompositeDisposable,
    schedulerProvider: SchedulerProvider,
    networkHelper: NetworkHelper,
    private val subject: PublishSubject<Pair<String?, Int>>,
    private val citiesRepository: CitiesRepository
) : BaseViewModel(compositeDisposable, schedulerProvider, networkHelper) {
    companion object{
        const val TAG = "SearchViewModel"
    }
    override fun onCreate() {

    }
    val searchCities = MutableLiveData<List<CityEntity>>()
    val newCities = MutableLiveData<List<CityEntity>>()
    var loadedItemsCount = 0
    val isLoading = MutableLiveData<Boolean>()
    init {
        compositeDisposable.addAll(
            subject
                .debounce(300, TimeUnit.MILLISECONDS)
                .doOnNext {
                    Log.d(TAG,"query = ${it.first} skip = ${it.second}")
                    isLoading.postValue(true)
                }
                .distinctUntilChanged()
                .switchMap{query->
                    return@switchMap getFilteredCities(query.first, query.second)
                        .doOnError {
                            messageStringId.postValue(R.string.database_error)
                            isLoading.postValue(false)
                        }
                        .onErrorReturn { e ->
                            Log.w(TAG, "failed to get cities ${e.localizedMessage}")
                            emptyList()
                        }
                }
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if(loadedItemsCount == 0){
                            newCities.postValue(it)
                        }else{
                            searchCities.postValue(it)
                        }
                        loadedItemsCount += it.size
                        isLoading.postValue(false)

                    },
                    {
                        messageStringId.postValue(R.string.database_error)
                        searchCities.postValue(emptyList())
                        isLoading.postValue(false)
                    }
                )
        )
    }

    private fun getFilteredCities(text:String?, skip:Int): Observable<List<CityEntity>> =
        citiesRepository.fetchCitiesFiltered(text, skip)

    val searchText = MutableLiveData<String>()
    fun onSearchTextChange(s: String) {
        searchText.postValue(s)
        loadedItemsCount = 0
    }



    fun doSearch() {
        subject.onNext(searchText.value to loadedItemsCount)
    }

    fun onLoadMore() {
        subject.onNext(searchText.value to loadedItemsCount)
    }

}
