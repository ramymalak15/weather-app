package com.demo.weather.ui.main.fragments.city_item

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.ui.base.BaseAdapter

class CityAdapter(
    val fragment: Fragment,
    private val cities: ArrayList<CityEntity>
) : BaseAdapter<CityEntity, CityItemViewHolder>(fragment.lifecycle, cities) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= CityItemViewHolder(parent, fragment)

    fun updateList(newList: List<CityEntity>) {
        val diffCallback = CitiesDiffCallback(cities, newList)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        cities.clear()
        cities.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    fun append(extra: List<CityEntity>) {
        val count = cities.size
        cities.addAll(extra)
        notifyItemRangeInserted(count, extra.size)
    }

    fun size() = cities.size
}