package com.demo.weather.ui.main.fragments.city_item

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.demo.weather.data.local.entitiy.CityEntity

class CitiesDiffCallback(private val oldList: List<CityEntity>, private val newList: List<CityEntity>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList.get(newItemPosition).id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition].name ==  newList[newPosition].name
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}