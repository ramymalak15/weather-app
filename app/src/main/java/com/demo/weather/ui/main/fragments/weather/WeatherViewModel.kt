package com.demo.weather.ui.main.fragments.weather

import androidx.lifecycle.MutableLiveData
import com.demo.weather.data.local.entitiy.ForecastEntity
import com.demo.weather.data.repository.CitiesRepository
import com.demo.weather.data.repository.ForecastRepository
import com.demo.weather.ui.base.BaseViewModel
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class WeatherViewModel(
    compositeDisposable: CompositeDisposable,
    schedulerProvider: SchedulerProvider,
    networkHelper: NetworkHelper,
    private val forecastRepository: ForecastRepository,
    private val citiesRepository: CitiesRepository
) : BaseViewModel(compositeDisposable, schedulerProvider, networkHelper) {
    override fun onCreate() {

    }

    val isLoading = MutableLiveData<Boolean>()
    val added = MutableLiveData<Boolean>()
    val forecastList = MutableLiveData<List<ForecastEntity>>()
    fun getForeCast(cityId: Long) {
        isLoading.postValue(true)
        if(networkHelper.isNetworkConnected()){
            compositeDisposable.add(
                forecastRepository.fetchForecastByCityId(cityId)
                    .flatMap {
                        forecastRepository.insertForecast(it.list, it.city.id)
                    }
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            getLocalForecast(cityId)
                        },
                        {
                            messageString.postValue(it.localizedMessage)
                            getLocalForecast(cityId)
                        }

                    )
            )
        }else{
            getLocalForecast(cityId)
        }
    }

    private fun getLocalForecast(cityId: Long) {
        compositeDisposable.addAll(
            forecastRepository.getLocalForeCast(cityId)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        forecastList.postValue(it)
                        isLoading.postValue(false)
                    },
                    {
                        messageString.postValue(it.localizedMessage)
                        isLoading.postValue(false)
                    }
                )
        )
    }

    fun setSelected(cityId: Long?) {
        cityId?.run {
            isLoading.postValue(true)
            compositeDisposable.add(
                citiesRepository.setCitySelected(cityId)
                    .subscribeOn(schedulerProvider.io())
                    .subscribe(
                        {
                            added.postValue(true)
                        },
                        {
                            messageString.postValue(it.localizedMessage)
                        }
                    )
            )
        }

    }
}