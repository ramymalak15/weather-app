package com.demo.weather.ui.main.fragments.weather.weather_item

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.demo.weather.data.local.entitiy.ForecastEntity
import com.demo.weather.ui.base.BaseItemViewModel
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class ForecastItemViewModel @Inject constructor(
    compositeDisposable: CompositeDisposable,
    schedulerProvider: SchedulerProvider,
    networkHelper: NetworkHelper
): BaseItemViewModel<ForecastEntity>(compositeDisposable, schedulerProvider, networkHelper){
    override fun onCreate() {

    }


    val date:LiveData<String> = Transformations.map(data){
        it.dtTxt.split(" ")[0]
    }
    val time:LiveData<String> = Transformations.map(data){
        it.dtTxt.split(" ")[1]
    }
    val feelsLike:LiveData<Double> = Transformations.map(data){
        it.main.feels_like
    }
    val humidity:LiveData<Int> = Transformations.map(data){
        it.main.humidity
    }
    val pressure:LiveData<Int> = Transformations.map(data){
        it.main.pressure
    }
    val temp:LiveData<Double> = Transformations.map(data){
        it.main.temp
    }
    val tempMinMax:LiveData<Pair<Double, Double>> = Transformations.map(data){
        it.main.temp_min to it.main.temp_max
    }
    val wind:LiveData<Pair<Double, Int>> = Transformations.map(data){
        it.wind.speed to it.wind.deg
    }

    val description:LiveData<String> = Transformations.map(data){
        it.weather.description
    }

}