package com.demo.weather.ui.main.fragments.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.demo.weather.databinding.SearchFragmentBinding
import com.demo.weather.di.component.FragmentComponent
import com.demo.weather.ui.base.BaseFragment
import com.demo.weather.ui.main.fragments.city_item.CityAdapter
import javax.inject.Inject
import javax.inject.Provider

class SearchFragment : BaseFragment<SearchViewModel, SearchFragmentBinding>() {

    @Inject
    lateinit var linearLayoutManager: Provider<LinearLayoutManager>

    @Inject
    lateinit var citiesRecyclerAdapter: CityAdapter

    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): SearchFragmentBinding = SearchFragmentBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) =
        fragmentComponent.inject(this)

    override fun setupView(view: View) {
        binding.searchRv.apply {
            layoutManager = linearLayoutManager.get()
            adapter = citiesRecyclerAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    layoutManager?.run {
                        if (this is LinearLayoutManager
                            && itemCount > 0
                            && itemCount == findLastVisibleItemPosition() + 1
                        ) viewModel.onLoadMore()
                    }
                }
            })
        }
        binding.searchSv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onSearchTextChange(query ?: "")
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onSearchTextChange(newText ?: "")
                return true
            }
        })
        viewModel.onSearchTextChange("")
    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.isLoading.observe(this, {
            binding.progress.visibility = if(it) View.VISIBLE else View.GONE
        })
        viewModel.searchText.observe(this, {
            it?.run {
                if (this != binding.searchSv.query.toString()) {
                    binding.searchSv.setQuery(this, false)
                }
                viewModel.doSearch()
            }
        })

        viewModel.newCities.observe(this, {
            citiesRecyclerAdapter.updateList(it)
        })
        viewModel.searchCities.observe(this, {
            citiesRecyclerAdapter.append(it)
        })
    }
}
