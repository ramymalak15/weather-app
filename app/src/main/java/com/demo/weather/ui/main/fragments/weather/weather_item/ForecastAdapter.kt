package com.demo.weather.ui.main.fragments.weather.weather_item

import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import com.demo.weather.data.local.entitiy.ForecastEntity
import com.demo.weather.ui.base.BaseAdapter

class ForecastAdapter(
    parentLifecycle: Lifecycle,
    private val forecasts: ArrayList<ForecastEntity>
) : BaseAdapter<ForecastEntity, ForecastItemViewHolder>(parentLifecycle, forecasts) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ForecastItemViewHolder(parent)
    fun updateAdapter(newList: List<ForecastEntity>) {
        forecasts.clear()
        forecasts.addAll(newList)
        notifyDataSetChanged()

    }

}