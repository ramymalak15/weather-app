package com.demo.weather.ui.main.fragments.cities

import androidx.lifecycle.MutableLiveData
import com.demo.weather.data.repository.CitiesRepository
import com.demo.weather.data.repository.ForecastRepository
import com.demo.weather.ui.base.BaseViewModel
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable

class CitiesViewModel(
    compositeDisposable: CompositeDisposable,
    schedulerProvider: SchedulerProvider,
    networkHelper: NetworkHelper,
    private val citiesRepository: CitiesRepository,
    private val forecastRepository: ForecastRepository
) : BaseViewModel(compositeDisposable, schedulerProvider, networkHelper) {


    override fun onCreate() {
    }

    val isLoading = MutableLiveData<Boolean>()
    val isNetworkConnected = MutableLiveData<Boolean>()
    val selectedCities = citiesRepository.fetchSelectedCities()

    val isEmpty = MutableLiveData<Boolean>()
    fun onEmptyCities() {
        isEmpty.postValue(selectedCities.value.isNullOrEmpty())
    }

    fun setDefault() {
        if(!networkHelper.isNetworkConnected()){
            isNetworkConnected.postValue(false)
            return
        }
        isLoading.postValue(true)
        compositeDisposable.addAll(
            citiesRepository.getCity("London", "GB")
                .flatMap {
                    forecastRepository.fetchForecastByCityId(it.id)
                }.flatMap { weatherResponse ->
                    citiesRepository.setCitySelected(
                        weatherResponse.city.id
                    ).flatMap {
                        forecastRepository.insertForecast(
                            weatherResponse.list,
                            weatherResponse.city.id
                        )
                    }.subscribeOn(schedulerProvider.io())
                }
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        isLoading.postValue(false)
                    },
                    {
                        isLoading.postValue(false)
                        messageString.postValue(it.localizedMessage)
                    }
                )
        )
    }

    fun setSelectedByLocation(longitude: Double, latitude: Double) {
        if(!networkHelper.isNetworkConnected()){
            isNetworkConnected.postValue(false)
            return
        }
        isLoading.postValue(true)
        compositeDisposable.addAll(
            forecastRepository.fetchForecastByLocation(latitude, longitude)
                .flatMap { weatherResponse ->
                    citiesRepository.setCitySelected(
                        weatherResponse.city.id
                    ).flatMap {
                        forecastRepository.insertForecast(
                            weatherResponse.list,
                            weatherResponse.city.id
                        )
                    }.subscribeOn(schedulerProvider.io())
                }
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        isLoading.postValue(false)
                    },
                    {
                        isLoading.postValue(false)
                        messageString.postValue(it.localizedMessage)
                    }
                )
        )
    }

}
