package com.demo.weather.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.demo.weather.WeatherApplication
import com.demo.weather.databinding.MainActivityBinding
import com.demo.weather.di.component.DaggerActivityComponent
import com.demo.weather.di.module.ActivityModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(MainActivityBinding.inflate(layoutInflater).root)
    }

    private fun injectDependencies() {
        DaggerActivityComponent
            .builder()
            .applicationComponent((application as WeatherApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()
            .inject(this)
    }
}