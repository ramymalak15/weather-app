package com.demo.weather.ui.main.fragments.weather

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.weather.R
import com.demo.weather.databinding.WeatherFragmentBinding
import com.demo.weather.di.component.FragmentComponent
import com.demo.weather.ui.base.BaseFragment
import com.demo.weather.ui.main.fragments.weather.weather_item.ForecastAdapter
import com.demo.weather.utils.Constants
import javax.inject.Inject
import javax.inject.Provider

class WeatherFragment : BaseFragment<WeatherViewModel, WeatherFragmentBinding>() {

    @Inject
    lateinit var linearLayoutManager: Provider<LinearLayoutManager>

    @Inject
    lateinit var forecastAdapter: ForecastAdapter

    override fun bindingInflater(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?
    ): WeatherFragmentBinding = WeatherFragmentBinding.inflate(layoutInflater)

    override fun injectDependencies(fragmentComponent: FragmentComponent) =
        fragmentComponent.inject(this)

    override fun setupView(view: View) {
        binding.weatherRv.apply {
            adapter = forecastAdapter
            layoutManager = linearLayoutManager.get()
        }
        val cityId = arguments?.getLong(Constants.CITY_ID)
        val cityName = arguments?.getString(Constants.CITY_NAME)
        val countryName = arguments?.getString(Constants.COUNTRY_NAME)
        binding.cityName.apply {
            text = cityName
            val id = resources.getIdentifier(
                "flag$countryName",
                "drawable",
                context.packageName
            )
            setCompoundDrawablesWithIntrinsicBounds(id, 0, 0, 0)
        }
        cityId?.run {
            viewModel.getForeCast(this)
        }
        val isAdd = arguments?.getBoolean(Constants.IS_ADD_CITY)
        binding.addCity.visibility = if(isAdd == true) View.VISIBLE else View.GONE
        binding.addCity.setOnClickListener {
            viewModel.setSelected(cityId)
        }

    }

    override fun setupObservers() {
        super.setupObservers()
        viewModel.added.observe(this, {
            if(it){
                findNavController().navigate(R.id.action_weatherFragment_to_citiesFragment)
            }
        })
        viewModel.forecastList.observe(this, {
            forecastAdapter.updateAdapter(it)
        })
        viewModel.isLoading.observe(this, {
            binding.progress.visibility = if(it) View.VISIBLE else View.GONE
        })
    }
}
