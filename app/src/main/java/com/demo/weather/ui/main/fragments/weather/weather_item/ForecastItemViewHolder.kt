package com.demo.weather.ui.main.fragments.weather.weather_item

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.demo.weather.R
import com.demo.weather.data.local.entitiy.ForecastEntity
import com.demo.weather.databinding.ForecastItemBinding
import com.demo.weather.di.component.ViewHolderComponent
import com.demo.weather.ui.base.BaseItemViewHolder

class ForecastItemViewHolder(parent: ViewGroup) :
    BaseItemViewHolder<ForecastEntity, ForecastItemViewModel, ForecastItemBinding>(
        ForecastItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        ),
        parent
    ) {

    override fun injectDependencies(viewHolderComponent: ViewHolderComponent) =
        viewHolderComponent.inject(this)

    override fun setupView(view: View) {

    }

    override fun setupObservers() {
        viewModel.date.observe(this, {
            it?.run {
                binding.dateTv.text = this
            }
        })
        viewModel.time.observe(this, {
            it?.run {
                binding.timeTv.text = this
            }
        })
        viewModel.temp.observe(this, {
            it?.run {
                binding.tempTv.text = itemView.context.getString(R.string.temp, this.toString())
            }
        })
        viewModel.tempMinMax.observe(this, {
            it?.run {
                binding.tempMinMaxTv.text = itemView.context.getString(R.string.tempMinMaxTv, this.second.toString(), this.first.toString())
            }
        })
        viewModel.description.observe(this, {
            it?.run {
                binding.weatherState.text = this
            }
        })
        viewModel.humidity.observe(this, {
            it?.run {
                binding.humidityProgress.progress = this
                binding.humidityTv.text = itemView.context.getString(R.string.humidity, this,"%")
            }
        })
        viewModel.feelsLike.observe(this, {
            it?.run {
                binding.feelsLike.text = itemView.context.getString(R.string.feelsLike, this.toString())
            }
        })
        viewModel.pressure.observe(this, {
            it?.run {
                binding.pressure.text = itemView.context.getString(R.string.pressure, this)
            }
        })
        viewModel.wind.observe(this, {
            it?.run {
                binding.wind.text = itemView.context.getString(R.string.wind, this.first.toString(), this.second.toString())
            }
        })
    }
}