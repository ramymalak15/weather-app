package com.demo.weather.ui.main.fragments.city_item

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.demo.weather.R
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.data.repository.CitiesRepository
import com.demo.weather.data.repository.ForecastRepository
import com.demo.weather.ui.base.BaseItemViewModel
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class CityItemViewModel @Inject constructor(
    compositeDisposable: CompositeDisposable,
    schedulerProvider: SchedulerProvider,
    networkHelper: NetworkHelper,
    private val citiesRepository: CitiesRepository,
    private val forecastRepository: ForecastRepository
): BaseItemViewModel<CityEntity>(compositeDisposable, schedulerProvider, networkHelper){
    override fun onCreate() {

    }

    companion object{
        const val TAG = "CityItemViewModel"
    }


    fun deleteCity() {
        compositeDisposable.addAll(
            citiesRepository.deleteCityById(cityId.value)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        messageStringId.postValue(R.string.deleted_successfully)
                    },
                    {
                        messageString.postValue(it.localizedMessage)
                    }
                )

        )
    }
    val temp = MutableLiveData<Double?>()
    fun getTemp(){
        compositeDisposable.addAll(
            forecastRepository.getTempByCityId(cityId.value)
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        temp.postValue(it)
                    },
                    {
                        temp.postValue(null)
                        Log.d(TAG, it.localizedMessage?:"")
                    }
                )
        )
    }

    val cityId: LiveData<Long> = Transformations.map(data){
        it.id
    }

    val city: LiveData<String> = Transformations.map(data){
        it.name
    }
    val country:LiveData<String> = Transformations.map(data){
        it.country.lowercase()
    }

}