package com.demo.weather.ui.main.fragments.city_item

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.demo.weather.R
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.databinding.CityItemBinding
import com.demo.weather.di.component.ViewHolderComponent
import com.demo.weather.ui.base.BaseItemViewHolder
import com.demo.weather.ui.main.fragments.cities.CitiesFragment
import com.demo.weather.ui.main.fragments.search.SearchFragment
import com.demo.weather.utils.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class CityItemViewHolder(parent: ViewGroup, private val fragment: Fragment) :
    BaseItemViewHolder<CityEntity, CityItemViewModel, CityItemBinding>(
        CityItemBinding.inflate(
            LayoutInflater.from(
                parent.context
            ), parent, false
        ),
        parent
    ) {

    override fun injectDependencies(viewHolderComponent: ViewHolderComponent) =
        viewHolderComponent.inject(this)

    override fun setupView(view: View) {
        view.setOnClickListener {
            fragment.findNavController().navigate(R.id.action_weatherFragment, Bundle().apply {
                putLong(Constants.CITY_ID, viewModel.cityId.value?:-1)
                putString(Constants.CITY_NAME, viewModel.city.value?:"")
                putString(Constants.COUNTRY_NAME, viewModel.country.value?:"")
                putBoolean(Constants.IS_ADD_CITY, fragment is SearchFragment)
            })
        }

        view.setOnLongClickListener {
            if(fragment is CitiesFragment){
                MaterialAlertDialogBuilder(itemView.context)
                    .setMessage(itemView.context.getString(R.string.delete_msg, "${viewModel.city.value}, ${viewModel.country.value?.uppercase()}"))
                    .setPositiveButton(itemView.context.getString(R.string.yes)) { dialog, _ ->
                        viewModel.deleteCity()
                        dialog.dismiss()
                    }
                    .setNegativeButton(itemView.context.getString(R.string.cancel_delete)){dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
                return@setOnLongClickListener true
            }
            return@setOnLongClickListener false


        }
    }

    override fun setupObservers() {
        viewModel.cityId.observe(this, {})
        viewModel.temp.observe(this, {
            if(it != null){
                binding.cityTemp.text = itemView.context.getString(R.string.temp, it.toString())
            }else{
                binding.cityTemp.text = ""
            }

        })
        viewModel.city.observe(this, {
            it?.run {
                binding.cityName.text = this
                viewModel.getTemp()
            }
        })

        viewModel.country.observe(this, {
            it?.run {
                val id = itemView.resources.getIdentifier(
                    "flag$this",
                    "drawable",
                    itemView.context.packageName
                )
                binding.cityName.setCompoundDrawablesWithIntrinsicBounds(id, 0, 0, 0)
            }
        })
    }
}