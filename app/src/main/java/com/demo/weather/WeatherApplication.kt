package com.demo.weather

import android.app.Application
import com.demo.weather.di.component.ApplicationComponent
import com.demo.weather.di.component.DaggerApplicationComponent
import com.demo.weather.di.module.ApplicationModule
import com.melegy.redscreenofdeath.RedScreenOfDeath

class WeatherApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
        RedScreenOfDeath.init(this)
    }

    private fun injectDependencies() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)
    }

    fun setComponent(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }
}