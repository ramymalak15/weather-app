package com.demo.weather.data.remote.response


import com.squareup.moshi.Json

data class Sys(
    @Json(name = "pod")
    val pod: String
)