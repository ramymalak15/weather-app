package com.demo.weather.data.local.doa

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.demo.weather.data.local.entitiy.CityEntity
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@Dao
interface CityDao {

    @Insert
    fun insert(vararg cityEntity: CityEntity)

    @Query("SELECT * FROM city_entity_table")
    fun getAll(): Single<List<CityEntity>>

    @Query("SELECT COUNT(*) FROM city_entity_table")
    fun getCount():Single<Int>

    @Query("SELECT * FROM city_entity_table WHERE selected != 1 AND name LIKE  :searchName LIMIT 10 OFFSET :skip")
    fun getFiltered(searchName: String, skip: Int): Observable<List<CityEntity>>

    @Query("SELECT * FROM city_entity_table WHERE selected = 1")
    fun getSelected(): LiveData<List<CityEntity>>

    @Query("UPDATE city_entity_table SET selected = 1 WHERE id = :cityId")
    fun setSelected(cityId: Long):Single<Int>

    @Query("SELECT * FROM city_entity_table WHERE name = :cityName AND country = :country LIMIT 1")
    fun getCity(cityName: String, country: String): Single<CityEntity>

    @Query("DELETE FROM city_entity_table WHERE id = :cityId")
    fun delete(cityId: Long?): Single<Int>
}