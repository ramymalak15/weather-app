package com.demo.weather.data.remote.response


import com.squareup.moshi.Json

data class WeatherResponse(
    @Json(name = "city")
    val city: City,
    @Json(name = "cnt")
    val cnt: Int,
    @Json(name = "cod")
    val cod: String,
    @Json(name = "list")
    val list: List<Forecast>,
    @Json(name = "message")
    val message: Int
)