package com.demo.weather.data.local.doa

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.demo.weather.data.local.entitiy.ForecastEntity
import io.reactivex.rxjava3.core.Single

@Dao
interface ForecastDao {
    @Insert
    fun insert(vararg forecastEntity: ForecastEntity): List<Long>

    @Query("SELECT * FROM forecast_entity_table WHERE city = :cityId")
    fun getByCityId(cityId: Long): Single<List<ForecastEntity>>

    @Query("DELETE FROM forecast_entity_table WHERE city = :cityId")
    fun deleteByCityId(cityId: Long)

    @Transaction
    fun insertAndDeleteInTransaction(cityId: Long, vararg entities: ForecastEntity): List<Long> {
        deleteByCityId(cityId)
        return insert(*entities)
    }
    @Query("SELECT `temp` FROM forecast_entity_table WHERE city = :cityId LIMIT 1")
    fun getTemp(cityId: Long?): Single<Double>
}