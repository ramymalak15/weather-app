package com.demo.weather.data.local.entitiy

import androidx.room.ColumnInfo

data class Coord(
    val lat: Double,
    val lon: Double
)