package com.demo.weather.data.repository

import com.demo.weather.data.local.DatabaseService
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.data.remote.NetworkService
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class CitiesRepository @Inject constructor(
    private val databaseService: DatabaseService
) {
    fun fetchCitiesFiltered(name: String?, skip: Int): Observable<List<CityEntity>> =
        databaseService.cityDao().getFiltered("%$name%", skip)

    fun fetchSelectedCities() = databaseService.cityDao().getSelected()
    fun setCitySelected(cityId: Long) = databaseService.cityDao().setSelected(cityId)

    fun getCity(cityName: String, country: String) =
        databaseService.cityDao().getCity(cityName, country)

    fun deleteCityById(cityId: Long?) = databaseService.cityDao().delete(cityId)


}
