package com.demo.weather.data.local.entitiy

import androidx.room.*
import com.demo.weather.data.remote.response.*

@Entity(tableName = "forecast_entity_table",foreignKeys = [ForeignKey(
    entity = CityEntity::class,
    parentColumns = arrayOf("id"),
    childColumns = arrayOf("city"),
    onDelete = ForeignKey.CASCADE
)])
data class ForecastEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "fid")
    val fid: Long,
    @ColumnInfo(index = true)
    val city: Long,
    @Embedded
    val clouds: Clouds,
    val dt: Int,
    val dtTxt: String,
    @Embedded
    val main: Main,
    val pop: Double,
    @Embedded
    val sys: Sys,
    val visibility: Int,
    @Embedded
    val weather: Weather,
    @Embedded
    val wind: Wind
)
