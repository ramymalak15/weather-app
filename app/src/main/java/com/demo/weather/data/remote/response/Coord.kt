package com.demo.weather.data.remote.response


import com.squareup.moshi.Json

data class Coord(
    @Json(name = "lat")
    val lat: Double,
    @Json(name = "lon")
    val lon: Double
)