package com.demo.weather.data.repository

import com.demo.weather.data.local.DatabaseService
import com.demo.weather.data.local.entitiy.ForecastEntity
import com.demo.weather.data.remote.NetworkService
import com.demo.weather.data.remote.response.Forecast
import com.demo.weather.data.remote.response.WeatherResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ForecastRepository @Inject constructor(
    private val networkService: NetworkService,
    private val databaseService: DatabaseService
) {

    fun insertForecast(forecastList: List<Forecast>, cityId: Long): Single<List<Long>> =
        Single.fromCallable {
            databaseService.forecastDao().insertAndDeleteInTransaction(
                cityId,
                *forecastList.map { forecast ->
                    ForecastEntity(
                        0,
                        cityId,
                        forecast.clouds,
                        forecast.dt,
                        forecast.dt_txt,
                        forecast.main,
                        forecast.pop,
                        forecast.sys,
                        forecast.visibility,
                        forecast.weather.first(),
                        forecast.wind
                    )
                }.toTypedArray()
            )
        }

    fun getLocalForeCast(cityId: Long) = databaseService.forecastDao().getByCityId(cityId)

    fun fetchForecastByLocation(lat: Double, lon: Double): Single<WeatherResponse> =
        networkService.getForecastByLocation(lat, lon)

    fun fetchForecastByCityId(cityId: Long) = networkService.getForecastByCityId(cityId)

    fun getTempByCityId(cityId: Long?) = databaseService.forecastDao().getTemp(cityId)


}
