package com.demo.weather.data.remote

import com.demo.weather.BuildConfig
import com.demo.weather.data.remote.response.WeatherResponse
import com.demo.weather.utils.Constants
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET("forecast")
    fun getForecastByLocation(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appid: String = BuildConfig.API_KEY,
        @Query("units") units:String = Constants.UNIT
    ): Single<WeatherResponse>


    @GET("forecast")
    fun getForecastByCityId(
        @Query("id") id: Long,
        @Query("appid") appid: String = BuildConfig.API_KEY,
        @Query("units") units:String = Constants.UNIT
    ): Single<WeatherResponse>

}