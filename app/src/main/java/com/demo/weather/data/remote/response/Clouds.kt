package com.demo.weather.data.remote.response


import com.squareup.moshi.Json

data class Clouds(
    @Json(name = "all")
    val all: Int
)