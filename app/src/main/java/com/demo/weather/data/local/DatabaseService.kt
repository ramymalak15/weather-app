package com.demo.weather.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.demo.weather.data.local.doa.CityDao
import com.demo.weather.data.local.doa.ForecastDao
import com.demo.weather.data.local.entitiy.CityEntity
import com.demo.weather.data.local.entitiy.ForecastEntity
import javax.inject.Singleton

@Singleton
@Database(entities = [CityEntity::class, ForecastEntity::class], version = 1, exportSchema = true)
abstract class DatabaseService: RoomDatabase(){
    abstract fun cityDao(): CityDao
    abstract fun forecastDao(): ForecastDao
}