package com.demo.weather.data.local.entitiy

import androidx.room.*

@Entity(tableName = "city_entity_table", indices = [
    Index(name = "city_name", value = ["name"])
])
data class CityEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Long,
    @Embedded
    val coord: Coord,
    val country: String,
    val name: String,
    val state: String,
    @ColumnInfo(name ="selected", defaultValue = "0")
    val selected: Boolean
){
    override fun toString(): String {
        return name
    }
}

