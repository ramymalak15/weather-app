package com.demo.weather.utils

object Constants {
    const val UNIT: String = "metric"
    const val CITY_ID = "CITY_ID"
    const val CITY_NAME = "CITY_NAME"
    const val COUNTRY_NAME = "COUNTRY_NAME"
    const val IS_ADD_CITY = "IS_ADD_CITY"
    const val MAX_CITY = 5
}