package com.demo.weather.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.demo.weather.ui.main.MainActivityViewModel
import com.demo.weather.utils.factory.ViewModelProviderFactory
import dagger.Module
import dagger.Provides
@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideMainActivityViewModel(): MainActivityViewModel = ViewModelProvider(
        activity, ViewModelProviderFactory(MainActivityViewModel::class) {
            MainActivityViewModel()
        }).get(MainActivityViewModel::class.java)

}