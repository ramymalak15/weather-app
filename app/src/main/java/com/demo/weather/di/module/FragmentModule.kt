package com.demo.weather.di.module

import android.content.Context
import android.location.LocationManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.weather.data.repository.CitiesRepository
import com.demo.weather.data.repository.ForecastRepository
import com.demo.weather.ui.main.fragments.cities.CitiesViewModel
import com.demo.weather.ui.main.fragments.city_item.CityAdapter
import com.demo.weather.ui.main.fragments.search.SearchViewModel
import com.demo.weather.ui.main.fragments.weather.WeatherViewModel
import com.demo.weather.ui.main.fragments.weather.weather_item.ForecastAdapter
import com.demo.weather.utils.factory.ViewModelProviderFactory
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlin.collections.ArrayList

@Module
class FragmentModule(private val fragment: Fragment) {

    @Provides
    fun provideCitiesRecyclerAdapter(): CityAdapter = CityAdapter(fragment, ArrayList())

    @Provides
    fun provideForecastAdapter(): ForecastAdapter = ForecastAdapter(fragment.lifecycle, ArrayList())

    @Provides
    fun provideLinearLayoutManager(): LinearLayoutManager = LinearLayoutManager(fragment.requireContext())

    @Provides
    fun provideCitiesViewModel(
        compositeDisposable: CompositeDisposable,
        schedulerProvider: SchedulerProvider,
        networkHelper: NetworkHelper,
        citiesRepository: CitiesRepository,
        forecastRepository: ForecastRepository
    ): CitiesViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(CitiesViewModel::class) {
            CitiesViewModel(compositeDisposable,schedulerProvider, networkHelper, citiesRepository,forecastRepository)
        }).get(CitiesViewModel::class.java)

    @Provides
    fun provideSearchViewModel(
        compositeDisposable: CompositeDisposable,
        schedulerProvider: SchedulerProvider,
        networkHelper: NetworkHelper,
        citiesRepository: CitiesRepository
    ): SearchViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(SearchViewModel::class) {
            SearchViewModel(compositeDisposable,schedulerProvider, networkHelper,PublishSubject.create(), citiesRepository)
        }).get(SearchViewModel::class.java)

    @Provides
    fun provideWeatherViewModel(
        compositeDisposable: CompositeDisposable,
        schedulerProvider: SchedulerProvider,
        networkHelper: NetworkHelper,
        forecastRepository: ForecastRepository,
        citiesRepository: CitiesRepository
    ): WeatherViewModel = ViewModelProvider(
        fragment, ViewModelProviderFactory(WeatherViewModel::class) {
            WeatherViewModel(compositeDisposable,schedulerProvider, networkHelper,forecastRepository, citiesRepository)
        }).get(WeatherViewModel::class.java)

    @Provides
    fun provideFusedLocationProviderClient(): FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(fragment.requireActivity())


    @Provides
    fun provideLocationManager(): LocationManager =
        fragment.requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager


}