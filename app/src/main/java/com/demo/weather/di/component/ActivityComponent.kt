package com.demo.weather.di.component

import com.demo.weather.di.ActivityScope
import com.demo.weather.di.module.ActivityModule
import com.demo.weather.ui.main.MainActivity
import dagger.Component

@ActivityScope
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(activity: MainActivity)
}