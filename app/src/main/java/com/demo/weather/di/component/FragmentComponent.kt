package com.demo.weather.di.component

import com.demo.weather.di.FragmentScope
import com.demo.weather.di.module.FragmentModule
import com.demo.weather.ui.main.fragments.cities.CitiesFragment
import com.demo.weather.ui.main.fragments.search.SearchFragment
import com.demo.weather.ui.main.fragments.weather.WeatherFragment
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [FragmentModule::class]
)
interface FragmentComponent {
    fun inject(citiesFragment: CitiesFragment)
    fun inject(searchFragment: SearchFragment)
    fun inject(weatherFragment: WeatherFragment)

}
