package com.demo.weather.di.component

import com.demo.weather.di.ViewModelScope
import com.demo.weather.di.module.ViewHolderModule
import com.demo.weather.ui.main.fragments.city_item.CityItemViewHolder
import com.demo.weather.ui.main.fragments.weather.weather_item.ForecastItemViewHolder
import dagger.Component

@ViewModelScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewHolderModule::class]
)
interface ViewHolderComponent {
    fun inject(cityItemViewHolder: CityItemViewHolder)
    fun inject(cityItemViewHolder: ForecastItemViewHolder)

}
