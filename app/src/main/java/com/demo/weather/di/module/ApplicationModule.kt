package com.demo.weather.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.demo.weather.BuildConfig
import com.demo.weather.WeatherApplication
import com.demo.weather.data.local.DatabaseService
import com.demo.weather.data.remote.NetworkService
import com.demo.weather.data.remote.Networking
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import com.demo.weather.utils.rx.SchedulerProviderImpl
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: WeatherApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    @Singleton
    fun provideNetworkService(): NetworkService = Networking.create(
        BuildConfig.BASE_URL,
        application.cacheDir,
        10L * 1024 * 1024
    )

    @Provides
    @Singleton
    fun provideDatabaseService(): DatabaseService = Room.databaseBuilder(
        application, DatabaseService::class.java,
        "${application.packageName.replace(".", "-")}-db"
    ).createFromAsset("sqlite.db")
//        .fallbackToDestructiveMigration()
        .build()

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = SchedulerProviderImpl()

    @Provides
    @Singleton
    fun provideNetworkHelper(): NetworkHelper = NetworkHelper(application)
}
