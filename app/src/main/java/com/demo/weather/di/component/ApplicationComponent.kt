package com.demo.weather.di.component

import android.app.Application
import android.content.Context
import com.demo.weather.WeatherApplication
import com.demo.weather.data.local.DatabaseService
import com.demo.weather.data.remote.NetworkService
import com.demo.weather.di.module.ApplicationModule
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import dagger.Component
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(app: WeatherApplication)

    fun getApplication(): Application

    fun getContext(): Context

    fun getCompositeDisposable(): CompositeDisposable

    fun getNetworkService(): NetworkService

    fun getDatabaseService(): DatabaseService

    fun getSchedulerProvider(): SchedulerProvider

    fun getNetworkHelper(): NetworkHelper
}