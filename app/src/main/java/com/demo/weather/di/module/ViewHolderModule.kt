package com.demo.weather.di.module

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.ViewModelProvider
import com.demo.weather.di.ViewModelScope
import com.demo.weather.ui.base.BaseItemViewHolder
import com.demo.weather.utils.factory.ViewModelProviderFactory
import com.demo.weather.utils.network.NetworkHelper
import com.demo.weather.utils.rx.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.CompositeDisposable

@Module
class ViewHolderModule(private val viewHolder: BaseItemViewHolder<*, *, *>) {
    @Provides
    @ViewModelScope
    fun provideLifecycleRegistry(): LifecycleRegistry = LifecycleRegistry(viewHolder)
}
